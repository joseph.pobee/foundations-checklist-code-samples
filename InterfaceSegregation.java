public class InterfaceSegregation {
    
}

interface Employee {
    public void clockIn();

    public void approveRequest();
}

class Manager implements Employee {
    @Override
    public void clockIn() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void approveRequest() {
        // TODO Auto-generated method stub
        
    }
}

class Analyst implements Employee {
    @Override
    public void approveRequest() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void clockIn() {
        // TODO Auto-generated method stub
        
    }
}

interface CanManage {
    public void approveRequest();
}

interface IEmployee {
    public void clockIn();
}