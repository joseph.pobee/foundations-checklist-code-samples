// Higher order functions
const applyOnTwo = (fn) => fn(2)

//Sorting using pure function
const pureSort = (arr) => {
    const sortAcc = (arr, output) => {
        if(arr.length == 0) return output;
        const min = Math.min(...arr);
        const minIndex = arr.indexOf(min);
        return sortAcc(removeAtIndexPure(arr, minIndex), [...output, min])
    }

    return sortAcc(arr, [])
}

const removeAtIndexPure = (arr, idx) => {
    if(idx == 0) return arr.slice(1, arr.length)
    if(idx == arr.length - 1) return arr.slice(0, arr.length - 1)
    const left = arr.slice(0, idx);
    const right = arr.slice(idx + 1)
    return [...left, ...right]
}

//Function composition
const multiplyBy2 = (num) => num * 2
const square = (num) => num ** 2

const squareAndMultiplyBy2 = (num) => multiplyBy2(square(num))

//currying
const addAll = (num1, num2, num3) => num1 + num2 + num3

const curry = (fn) => (a) => (b) => (c) => fn(a,b,c)

const curriedAdd = curry(addAll)

const addWithTwo = curriedAdd(2);

const result = addWithTwo(2)(3)