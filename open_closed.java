class Card {
    private String type;

    public Card(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}

class PaymentProvider {
    public void makePayment(Card card) {
        if(card.getType().equals("Visa")) {
            System.out.println("making visa payment");
        } else if (card.getType().equals("Mastercard")) {
            System.out.println("Making Mastercard payment");
        }
    }
}

interface IPaymentProvider {
    public void pay();
}

class VisaPaymentProvider implements IPaymentProvider {
    public void pay() {
        System.out.println("Making Visa Payment");
    }
}

class MastercardPaymentProvider implements IPaymentProvider {
    @Override
    public void pay() {
        System.out.println("Mastercard Payment");
    }
}