class DistanceCalculator:
    def __init__(self, name):
        self.name = name

    def calculate_distance(self, distance_in_meters):
        # Distance calculator has two reasons to change
        # 1. if the method or formula for computing distance changes
        # 2. if the desired formatting of the message changes
        print(f"=== Distance is {distance_in_meters} ===")
        
