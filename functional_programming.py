def addAll(num1, num2, num3):
    return num1 + num2 + num3

def curry(fn):
    def f1(a):
        def f2(b):
            def f3(c):
                return fn(a,b,c)
            return f3
        return f2
    return f1

curriedAdd = curry(addAll)

print(curriedAdd(1)(2)(3))