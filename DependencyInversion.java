public class DependencyInversion {
    public static void main(String[] args) {
        new NewDBQuery().create(new MongoConnection());
        new NewDBQuery().create(new PostgresConnection());
    }
    
}

class MySQLConnection {
    public void connect() {
        System.out.println("Connecting to database");
    }
}

class DatabaseQuery {
    public void create(MySQLConnection connection) {
        connection.connect();
    }
}

interface DBConnection {
    public void connect();
}

class MongoConnection implements DBConnection {
    @Override
    public void connect() {
        // TODO Auto-generated method stub
        
    }
}

class PostgresConnection implements DBConnection {
    @Override
    public void connect() {
        // TODO Auto-generated method stub
        
    }
}

class NewDBQuery {
    public void create(DBConnection connection) {
        connection.connect();
    }
}

